class ArticlesController < ApplicationController
  include ArticlesHelper
  
  def index
    @articles = Article.all
  end
  
  def show
    @article = Article.find(params[:id])
    @comment = Comment.new
  end
  
  def new
    @article = Article.new
  end
  
  def create
    @article = Article.new(article_params)
    if(@article.save)    
      flash.notice = "Article created!"
    
      redirect_to article_path(@article)
    else
      render :new
    end
  end
  
  def destroy
    Article.find(params[:id]).destroy
    
    redirect_to articles_path
  end
  
  def edit
    @article = Article.find(params[:id])
  end
  
  def update
    @article = Article.find(params[:id])
    if(@article.update_attributes(article_params))
      flash.notice = "Article updated!"
    
      redirect_to article_path(@article)
    else
      render :edit
    end
  end
end