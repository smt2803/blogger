class TagsController < ApplicationController
  def show
    @tag = Tag.find(params[:id])
  end
  
  def index
    @tag = Tag.all
  end

  def destroy
    @tag = Tag.find(params[:id]).destroy

    redirect_to tags_path(@index)

    flash.notice = "Article '#{@tag.name}' deleted."
  end
end
